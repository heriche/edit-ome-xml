## Editing OME-XML tags

This perl script edits a XML tag from the provided XML file. It finds all occurrences of the tag and replaces their values with those from a given list or if a target attribute is given, replaces the values of the attribute of the target tag.

## Use case
This script was written to process the extracted XML header of an OME-TIFF file.
The XML can be extracted from, and inserted back into, an OME-TIFF file using the Bio-Formats tiffcomment tool (https://docs.openmicroscopy.org/bio-formats/latest/users/comlinetools/):

```
# Extract the XML into a file
tiffcomment image.ome.tiff > image.ome.xml

# Replace XML in ome-tiff file
tiffcomment -set new.ome.xml image.ome.tiff
```

## Dependencies
The script requires the perl module XML::Twig.
To install it:

```
PERL_MM_USE_DEFAULT=1 perl -MCPAN -e "install XML::Twig"
```
