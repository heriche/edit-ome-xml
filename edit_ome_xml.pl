#!/usr/bin/env perl

use warnings;
use strict;
use XML::Twig;
use Getopt::Long;

my ($help, $xml, $tag, $attribute, $values);
GetOptions ( 'help'=>\$help,
	     'xml=s' => \$xml,
	     'tag=s' => \$tag,
	     'attribute=s' => \$attribute,
	     'values=s' => \$values
           );
usage() if ($help || !$xml || !$tag || !$values);
if (!$xml) {
  die "\nERROR: Need an ome.xml file.\n";
}
if (!$tag) {
  die "\nERROR: Need a target tag name.\n";
}
if (!$values) {
  die "\nERROR: Need replacement values for the target tag.\n";
}

my @values;
if (-f $values)  {
  # Values are in a file
  open (FH, $values) or die "Can't open file $values: $!\n";
  @values = (<FH>);
  close FH;
  foreach my $line(@values) {
    # Remove end of line characters taking into account non-*nix ones
    $line=~s/\R+//g;
  }
}
else { # Assume comma-separated list argument
  @values = split(/\s*,\s*/, $values);
}

# Slurp in the whole XML file
my $twig = XML::Twig->new();
$twig->parsefile($xml);

# Find all occurrences of the target tag
my @list  = $twig->findnodes("//$tag");

if ($#values != $#list) {
  die "\nERROR: The number of values provided isn't equal to the number of occurrences of the target tag.\n";
}

my $counter = 0;
foreach ($twig->findnodes("//$tag")) {
  if ($attribute) {
    $_->set_att( $attribute => $values[$counter] );
  }
  else {
    $_->set_text( $values[$counter] );
  }
    $counter++;
  }
}

# Output the new XML tree
$twig->print;

exit;


sub usage {
  print STDERR<<"END";

    This program edits a XML tag from the extracted XML header of an OME-TIFF
    file. It finds all occurrences of the tag and replaces their values with 
    those from a given list or if a target attribute is given, replaces the
    values of the attribute of the target tag.
    The XML can be extracted from, and inserted back into, an OME-TIFF file
    using the Bio-Formats tiffcomment tool.

    Usage: $0 [-help] -xml file.ome.xml -tag tag_name -attribute tag_attribute -values /path/to/values_file.txt

     -h[elp]        : This message
     -x[ml]         : XML file extracted from the header of an OME-TIFF image.
     -t[ag]         : Target tag name
     -a[ttribute]   : (optional) Target attribute name. 
                      If given, the tag attribute values will be replaced otherwise 
                      the tag values are replaced.
     -v[alues]      : List of replacement values for the target tag.
                      There must be one value for each occurrence of the tag 
                      in the same order as the tag occurrences.
                      The argument can be a comma-separated list of values or
                      the name of a plain text file containing one value per line.

    example: $0 -x scan.ome.xml -t Channel -a Name -v "EGFR,ZWINT10,AURKB"

END

  exit;
}
